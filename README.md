# youtube-api

Fetch bulk YouTube video metadata using the YouTube API with a basic user interface.

## Usage
Add Google Cloud API key to app.js. Make sure to enable the YouTube data API.
Host and open the project on a server.

## Output
Array of playlistItem objects as detailed in the [YouTube API documentation](https://developers.google.com/youtube/v3/docs/playlistItems#resource). Downloads as JSON.

## Multiple channels at once
Run `_getMultipleChannels()` in the browser console. Input an array of objects for each YouTube channel with the following format:
* `type` to get `forUsername`,`id`, or `playlist`
* `name` for the username or user/playlist id