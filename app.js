const key = 'API-KEY';

const typeSelect = document.getElementById('typeSelect');
const nameInput = document.getElementById('nameInput');
const requestButton = document.getElementById('requestButton');
const progress = document.getElementById('progress');
const downloadButton = document.getElementById('downloadButton');
const downloadAllButton = document.getElementById('downloadAllButton');
const videos = document.getElementById('videos');

var currentName;
var currentVideos;

var allChannels = {};

function _getMultipleChannels(channels) {
	gapi.load('client', async () => {
		for(const c of channels) {
			const res = await getChannel(c.type, c.name);

			Object.defineProperty(allChannels, c.name, {
				value: res,
				enumerable: true
			});

			downloadAllButton.hidden = false;
		}
	});
}

requestButton.onclick = () => {
	gapi.load('client', async () => {
		const type = typeSelect.value;
		
		currentName = nameInput.value;
		if(type == 'forUsername') currentName = currentName.toLowerCase();

		currentVideos = await getChannel(type, currentName);
	});
}

async function getChannel(type, name) {
	downloadButton.hidden = true;

	var playlist;

	var totalVideos;
	var totalPages;

	var output;

	return await gapi.client.init({
		'apiKey': key,
	}).then(async () => {
		if(type == 'playlist') {
			videos.innerHTML = `<h2>Latest videos</h2>`
			return name;
		}
		return await gapi.client.request({
			'path': `https://content-youtube.googleapis.com/youtube/v3/channels?${type}=${name}&maxResults=50&part=contentDetails&part=snippet`,
		}).then(res => {
			videos.innerHTML = `<h2>${res.result.items[0].snippet.title}: Latest videos<h2>`;
			return res.result.items[0].contentDetails.relatedPlaylists.uploads;
		}).catch(error => {
			progress.innerText = `Error: Invalid ${typeSelect.selectedOptions[0].innerText}`;
			throw error;
		});
	}).then(async res => {
		playlist = res;

		return await gapi.client.request({
		'path': `https://content-youtube.googleapis.com/youtube/v3/playlistItems?playlistId=${playlist}&part=snippet&maxResults=50`,
		}).then(res => {
			res.result.items.forEach(video => {
				videos.innerHTML += `<h3><a href="https://youtube.com/watch?v=${video.snippet.resourceId.videoId}">${video.snippet.title}</a></h3>${video.snippet.publishedAt}<br><br>`;
			});

			totalVideos = res.result.pageInfo.totalResults;
			totalPages = Math.ceil(totalVideos/50);

			return res.result;
		}, error => {
			if(type == 'playlist') {
				progress.innerText = 'Error: Invalid Playlist';
			} else {
				progress.innerText = 'Error: YouTube returned invalid uploads playlist';
			}
			throw error;
		});
	}).then(async latestPage => {

		output = latestPage.items;

		for(let i = 1; i < totalPages; i++) {
			progress.innerText = `Loading... ${i*50}/${totalVideos}`

			await gapi.client.request({
				'path': `https://content-youtube.googleapis.com/youtube/v3/playlistItems?playlistId=${playlist}&part=snippet&maxResults=50&pageToken=${latestPage.nextPageToken}`,
			}).then(res => {
				latestPage = res.result;
				latestPage.items.forEach(item => {
					output.push(item);
				});
			});
		}

		progress.innerText = '';
		downloadButton.innerText = `Download ${totalVideos} videos`;
		downloadButton.hidden = false;

		return output;
	}).catch(error => {
		if(!progress.innerText.includes('Error')) progress.innerText = 'Unexpected error';
		console.error(error);
	});
}

downloadButton.onclick = () => {
	download(JSON.stringify(currentVideos), currentName, 'text/plain');
}

downloadAllButton.onclick = () => {
	download(JSON.stringify(allChannels), 'multipleChannels', 'text/plain');
}

function download(data, filename, type) {
    var file = new Blob([data], {type: type});
	var a = document.createElement("a"),
		url = URL.createObjectURL(file);
	a.href = url;
	a.download = filename;
	document.body.appendChild(a);
	a.click();
	setTimeout(function() {
		document.body.removeChild(a);
		window.URL.revokeObjectURL(url);  
	}, 0); 
}